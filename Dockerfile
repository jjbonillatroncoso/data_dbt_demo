FROM ghcr.io/dbt-labs/dbt-core:1.7.2
#RUN pip install dbt-databricks
RUN apt update
RUN apt-get install python-dev libsasl2-dev gcc curl jq -y
#RUN pip install dbt-spark[PyHive]
RUN pip install dbt-snowflake 
RUN pip install dbt-glue
RUN pip install dbt-athena-community
RUN pip install dbt-redshift
RUN pip3 install dbt-duckdb


