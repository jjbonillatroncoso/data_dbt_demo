{{ dbt_utils.date_spine(
    datepart="day",
    start_date="to_date('01/01/2022', 'mm/dd/yy')",
    end_date="dateadd(week,1,current_date)"
    )
}}