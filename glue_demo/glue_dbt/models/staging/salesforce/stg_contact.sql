{{ config(
    unique_key='id'
)}}
WITH RankedRecords as (
SELECT
id,                  
isdeleted,           
masterrecordid,      
accountid,           
lastname,            
firstname,           
salutation,          
name,                
phone,               
fax,                 
mobilephone,         
homephone,           
otherphone,          
assistantphone,      
reportstoid,         
email,               
title,               
department,          
assistantname,       
leadsource,          
birthdate,           
description,         
ownerid,             
createddate,         
createdbyid,         
lastmodifieddate,    
lastmodifiedbyid,    
systemmodstamp,      
lastactivitydate,    
lastcurequestdate,   
lastcuupdatedate,    
lastvieweddate,      
lastreferenceddate,  
emailbouncedreason,  
emailbounceddate,    
isemailbounced,      
photourl,            
jigsaw,              
jigsawcontactid,     
cleanstatus,         
individualid,        
level__c,            
languages__c,        
_sdc_extracted_at,   
_sdc_received_at,    
_sdc_batched_at,     
_sdc_deleted_at,     
_sdc_sequence,       
_sdc_table_version,  
_sdc_started_at,
    ROW_NUMBER() OVER (
      PARTITION BY id
      ORDER BY _sdc_extracted_at DESC 
    ) as rn 
    FROM {{ source ('salesforce', 'contact') }}
)
SELECT 
*
FROM RankedRecords
WHERE rn = 1;