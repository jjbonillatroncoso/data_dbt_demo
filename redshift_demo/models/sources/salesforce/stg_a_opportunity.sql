{{ config(materialized='view', bind=False) }}


with source as (
    SELECT * FROM {{ source ('salesforce', 'opportunity') }}
)

select * from source