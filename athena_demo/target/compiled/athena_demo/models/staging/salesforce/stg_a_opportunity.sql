
WITH RankedRecords as (
SELECT
CAST(_sdc_extracted_at AS timestamp(6)) AS _sdc_extracted_at,
CAST(_sdc_received_at AS timestamp(6)) AS _sdc_received_at,
CAST(_sdc_batched_at AS timestamp(6)) AS _sdc_batched_at,
CAST(_sdc_deleted_at AS timestamp(6)) AS _sdc_deleted_at,
"id",
  "isdeleted",
  "accountid",
  "isprivate",
  "name",
  "description",
  "stagename",
  "amount",
  "probability",
  "expectedrevenue",
  "totalopportunityquantity",
  "closedate",
  "type",
  "nextstep",
  "leadsource",
  "isclosed",
  "iswon",
  "forecastcategory",
  "forecastcategoryname",
  "campaignid",
  "hasopportunitylineitem",
  "pricebook2id",
  "ownerid",
  "createddate",
  "createdbyid",
  "lastmodifieddate",
  "lastmodifiedbyid",
  "systemmodstamp",
  "lastactivitydate",
  "pushcount",
  "laststagechangedate",
  "fiscalquarter",
  "fiscalyear",
  "fiscal",
  "contactid",
  "lastvieweddate",
  "lastreferenceddate",
  "hasopenactivity",
  "hasoverduetask",
  "lastamountchangedhistoryid",
  "lastclosedatechangedhistoryid",
  "deliveryinstallationstatus__c",
  "trackingnumber__c",
  "ordernumber__c",
  "currentgenerators__c",
  "maincompetitors__c",
  "_sdc_sequence",
  "_sdc_table_version",
  "_sdc_started_at",
    ROW_NUMBER() OVER (
      PARTITION BY id
      ORDER BY _sdc_extracted_at DESC 
    ) as rn 
    FROM "awsdatacatalog"."salesforce"."opportunity"
)
SELECT 
*
FROM RankedRecords
WHERE rn = 1;