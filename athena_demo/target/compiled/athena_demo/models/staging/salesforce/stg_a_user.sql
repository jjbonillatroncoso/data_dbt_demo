
WITH RankedRecords as (
SELECT
CAST(_sdc_extracted_at AS timestamp(6)) AS _sdc_extracted_at,
CAST(_sdc_received_at AS timestamp(6)) AS _sdc_received_at,
CAST(_sdc_batched_at AS timestamp(6)) AS _sdc_batched_at,
CAST(_sdc_deleted_at AS timestamp(6)) AS _sdc_deleted_at,
"id",
  "username",
  "lastname",
  "firstname",
  "name",
  "companyname",
  "division",
  "department",
  "title",
  "street",
  "city",
  "state",
  "postalcode",
  "country",
  "latitude",
  "longitude",
  "geocodeaccuracy",
  "address",
  "email",
  "emailpreferencesautobcc",
  "emailpreferencesautobccstayintouch",
  "emailpreferencesstayintouchreminder",
  "senderemail",
  "sendername",
  "signature",
  "stayintouchsubject",
  "stayintouchsignature",
  "stayintouchnote",
  "phone",
  "fax",
  "mobilephone",
  "alias",
  "communitynickname",
  "badgetext",
  "isactive",
  "timezonesidkey",
  "userroleid",
  "localesidkey",
  "receivesinfoemails",
  "receivesadmininfoemails",
  "emailencodingkey",
  "profileid",
  "usertype",
  "languagelocalekey",
  "employeenumber",
  "delegatedapproverid",
  "managerid",
  "lastlogindate",
  "lastpasswordchangedate",
  "createddate",
  "createdbyid",
  "lastmodifieddate",
  "lastmodifiedbyid",
  "systemmodstamp",
  "numberoffailedlogins",
  "offlinetrialexpirationdate",
  "offlinepdatrialexpirationdate",
  "userpermissionsmarketinguser",
  "userpermissionsofflineuser",
  "userpermissionscallcenterautologin",
  "userpermissionssfcontentuser",
  "userpermissionsknowledgeuser",
  "userpermissionsinteractionuser",
  "userpermissionssupportuser",
  "userpermissionsjigsawprospectinguser",
  "userpermissionssiteforcecontributoruser",
  "userpermissionssiteforcepublisheruser",
  "userpermissionsworkdotcomuserfeature",
  "forecastenabled",
  "userpreferencesactivityreminderspopup",
  "userpreferenceseventreminderscheckboxdefault",
  "userpreferencestaskreminderscheckboxdefault",
  "userpreferencesremindersoundoff",
  "userpreferencesdisableallfeedsemail",
  "userpreferencesdisablefollowersemail",
  "userpreferencesdisableprofilepostemail",
  "userpreferencesdisablechangecommentemail",
  "userpreferencesdisablelatercommentemail",
  "userpreferencesdisprofpostcommentemail",
  "userpreferencescontentnoemail",
  "userpreferencescontentemailasandwhen",
  "userpreferencesapexpagesdevelopermode",
  "userpreferencesreceivenonotificationsasapprover",
  "userpreferencesreceivenotificationsasdelegatedapprover",
  "userpreferenceshidecsngetchattermobiletask",
  "userpreferencesdisablementionspostemail",
  "userpreferencesdismentionscommentemail",
  "userpreferenceshidecsndesktoptask",
  "userpreferenceshidechatteronboardingsplash",
  "userpreferenceshidesecondchatteronboardingsplash",
  "userpreferencesdiscommentafterlikeemail",
  "userpreferencesdisablelikeemail",
  "userpreferencessortfeedbycomment",
  "userpreferencesdisablemessageemail",
  "userpreferenceshidelegacyretirementmodal",
  "userpreferencesjigsawlistuser",
  "userpreferencesdisablebookmarkemail",
  "userpreferencesdisablesharepostemail",
  "userpreferencesenableautosubforfeeds",
  "userpreferencesdisablefilesharenotificationsforapi",
  "userpreferencesshowtitletoexternalusers",
  "userpreferencesshowmanagertoexternalusers",
  "userpreferencesshowemailtoexternalusers",
  "userpreferencesshowworkphonetoexternalusers",
  "userpreferencesshowmobilephonetoexternalusers",
  "userpreferencesshowfaxtoexternalusers",
  "userpreferencesshowstreetaddresstoexternalusers",
  "userpreferencesshowcitytoexternalusers",
  "userpreferencesshowstatetoexternalusers",
  "userpreferencesshowpostalcodetoexternalusers",
  "userpreferencesshowcountrytoexternalusers",
  "userpreferencesshowprofilepictoguestusers",
  "userpreferencesshowtitletoguestusers",
  "userpreferencesshowcitytoguestusers",
  "userpreferencesshowstatetoguestusers",
  "userpreferencesshowpostalcodetoguestusers",
  "userpreferencesshowcountrytoguestusers",
  "userpreferenceshides1browserui",
  "userpreferencesdisableendorsementemail",
  "userpreferencespathassistantcollapsed",
  "userpreferencescachediagnostics",
  "userpreferencesshowemailtoguestusers",
  "userpreferencesshowmanagertoguestusers",
  "userpreferencesshowworkphonetoguestusers",
  "userpreferencesshowmobilephonetoguestusers",
  "userpreferencesshowfaxtoguestusers",
  "userpreferencesshowstreetaddresstoguestusers",
  "userpreferenceslightningexperiencepreferred",
  "userpreferencespreviewlightning",
  "userpreferenceshideenduseronboardingassistantmodal",
  "userpreferenceshidelightningmigrationmodal",
  "userpreferenceshidesfxwelcomemat",
  "userpreferenceshidebiggerphotocallout",
  "userpreferencesglobalnavbarwtshown",
  "userpreferencesglobalnavgridmenuwtshown",
  "userpreferencescreatelexappswtshown",
  "userpreferencesfavoriteswtshown",
  "userpreferencesrecordhomesectioncollapsewtshown",
  "userpreferencesrecordhomereservedwtshown",
  "userpreferencesfavoritesshowtopfavorites",
  "userpreferencesexcludemailappattachments",
  "userpreferencessuppresstasksfxreminders",
  "userpreferencessuppresseventsfxreminders",
  "userpreferencespreviewcustomtheme",
  "userpreferenceshascelebrationbadge",
  "userpreferencesuserdebugmodepref",
  "userpreferencessrhoverrideactivities",
  "userpreferencesnewlightningreportrunpageenabled",
  "userpreferencesreverseopenactivitiesview",
  "userpreferencesshowterritorytimezoneshifts",
  "userpreferencesnativeemailclient",
  "contactid",
  "accountid",
  "callcenterid",
  "extension",
  "federationidentifier",
  "aboutme",
  "fullphotourl",
  "smallphotourl",
  "isextindicatorvisible",
  "outofofficemessage",
  "mediumphotourl",
  "digestfrequency",
  "defaultgroupnotificationfrequency",
  "jigsawimportlimitoverride",
  "lastvieweddate",
  "lastreferenceddate",
  "bannerphotourl",
  "smallbannerphotourl",
  "mediumbannerphotourl",
  "isprofilephotoactive",
  "individualid",
  "_sdc_sequence",
  "_sdc_table_version",
  "_sdc_started_at",
    ROW_NUMBER() OVER (
      PARTITION BY id
      ORDER BY _sdc_extracted_at DESC 
    ) as rn 
    FROM "awsdatacatalog"."salesforce"."user"
)
SELECT 
*
FROM RankedRecords
WHERE rn = 1;