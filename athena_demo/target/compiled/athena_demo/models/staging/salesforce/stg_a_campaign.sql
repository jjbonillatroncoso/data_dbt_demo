
WITH RankedRecords as (
SELECT
CAST(_sdc_extracted_at AS timestamp(6)) AS _sdc_extracted_at,
CAST(_sdc_received_at AS timestamp(6)) AS _sdc_received_at,
CAST(_sdc_batched_at AS timestamp(6)) AS _sdc_batched_at,
CAST(_sdc_deleted_at AS timestamp(6)) AS _sdc_deleted_at,
"id",
  "isdeleted",
  "name",
  "parentid",
  "type",
  "status",
  "startdate",
  "enddate",
  "expectedrevenue",
  "budgetedcost",
  "actualcost",
  "expectedresponse",
  "numbersent",
  "isactive",
  "description",
  "numberofleads",
  "numberofconvertedleads",
  "numberofcontacts",
  "numberofresponses",
  "numberofopportunities",
  "numberofwonopportunities",
  "amountallopportunities",
  "amountwonopportunities",
  "ownerid",
  "createddate",
  "createdbyid",
  "lastmodifieddate",
  "lastmodifiedbyid",
  "systemmodstamp",
  "lastactivitydate",
  "lastvieweddate",
  "lastreferenceddate",
  "campaignmemberrecordtypeid",
  "_sdc_sequence",
  "_sdc_table_version",
  "_sdc_started_at",
    ROW_NUMBER() OVER (
      PARTITION BY id
      ORDER BY _sdc_extracted_at DESC 
    ) as rn 
    FROM "awsdatacatalog"."salesforce"."campaign"
)
SELECT 
*
FROM RankedRecords
WHERE rn = 1;