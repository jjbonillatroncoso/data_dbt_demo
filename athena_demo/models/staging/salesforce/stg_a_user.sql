{{ config(
    unique_key='id',
    update_condition='src.lastmodifieddate > target.lastmodifieddate and src._sdc_started_at > target._sdc_started_at'

)}}
WITH RankedRecords as (
SELECT
CAST(_sdc_extracted_at AS timestamp(6)) AS _sdc_extracted_at,
CAST(_sdc_received_at AS timestamp(6)) AS _sdc_received_at,
CAST(_sdc_batched_at AS timestamp(6)) AS _sdc_batched_at,
CAST(_sdc_deleted_at AS timestamp(6)) AS _sdc_deleted_at,
{{  dbt_utils.star(from=source('salesforce', 'user'), except=['_sdc_extracted_at','_sdc_received_at','_sdc_batched_at','_sdc_deleted_at']) }},
    ROW_NUMBER() OVER (
      PARTITION BY id
      ORDER BY _sdc_extracted_at DESC 
    ) as rn 
    FROM {{ source ('salesforce', 'user') }}
)
SELECT 
*
FROM RankedRecords
WHERE rn = 1;